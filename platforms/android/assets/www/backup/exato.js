angular.module('app', ['ngCookies','ui.utils.masks']);



function ExatoCtrl($scope,$cookieStore) {
    
    $scope.itens = [
        {unidade: '', undValor: ''},
        {unidade: '', undValor: ''}
    ];
    
    
    var itensCookie = $cookieStore.get('itensCookie');
   
    if( typeof itensCookie != "undefined"){
        $scope.itens = itensCookie;                
     }
    
        
    $scope.salvarCookie = function() {
      $cookieStore.put('itensCookie',$scope.itens);
    };
    
     $scope.addItem = function() {
               
                $scope.itens.push({unidade: '', undValor: ''});

    };
    
    $scope.mostrar = function(item) {

        if(angular.isNumber(item.unidade) && item.unidade!=0)
        {
            $scope.ehMenor(item);
            return true;
        }
        
        return false;
        
    };
    
    
    $scope.limpar = function(){
        
        $scope.itens = [
            {unidade: '', undValor: ''},
            {unidade: '', undValor: ''}
        ];
        
        $scope.salvarCookie();
        
    };
    
    $scope.ehMenor = function(item) {

        $scope.salvarCookie();
        
        $scope.myColor = '#557b23';
          
        var rsUnd = item.undValor/item.unidade;
        
        var lenList = $scope.itens.length;
        
        for(var i=0; i<lenList; i++){
            
            var rsUndIt = $scope.itens[i].undValor/$scope.itens[i].unidade;
            
        
            if( rsUnd>rsUndIt){
                $scope.myColor = 'red';
            }
            
            
            
        }
        
        
    };
    
}
